package com.jmendozadi;

public class Circle extends Geometric {

    public Circle(String figurename) {
        super(figurename, 0, 0);
    }

    @Override
    public void areaCalculation()
    {

    }

    @Override
    public void perimeterCalculation() {

    }

    @java.lang.Override
    public void perimeterCalculationTwo() {

    }

    @Override
    public void radiusCalculation() {
        radius=this.diameter/2;
    }

    @Override
    public double areaCalculatorWithRope(double rope, double distanceCenter){
        double radio = Math.sqrt(Math.pow((rope / 2), 2) + Math.pow(distanceCenter, 2));
        double result = Math.PI * Math.pow(radio, 2);
        return result;
    }

}
